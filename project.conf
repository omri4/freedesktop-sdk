name: freedesktop-sdk

format-version: 12

aliases:
  freedesktop: https://gitlab.freedesktop.org/
  freedesktop-old: https://anongit.freedesktop.org/git/
  ftp_gnu_org: https://ftp.gnu.org/gnu/
  savannah: https://git.savannah.gnu.org/git/
  github: https://github.com/
  gnome: https://gitlab.gnome.org/GNOME/
  perl5: https://perl5.git.perl.org/perl.git

mirrors:
  - name: kernel_org
    aliases:
      ftp_gnu_org:
      - https://mirrors.kernel.org/gnu/
  - name: github_mirrors
    aliases:
      perl5:
      - https://github.com/Perl/perl5.git

element-path: elements

fail-on-overlap: true

variables:
  builddir: bst_build_dir
  conf-deterministic: |
    --enable-deterministic-archives
  conf-link-args: |
    --enable-shared \
    --disable-static
  conf-host: |
    --host=%{host-triplet}
  conf-build: |
    --build=%{build-triplet}
  host-triplet: "%{triplet}"
  build-triplet: "%{triplet}"
  sbindir: "%{bindir}"
  sysconfdir: "%{prefix}/etc"
  localstatedir: "%{prefix}/var"
  branch: "18.08"
  lib: "lib/%{gcc_triplet}"
  indep-libdir: "%{prefix}/lib"
  debugdir: "%{indep-libdir}/debug"
  sourcedir: "%{debugdir}/source"
  gcc_triplet: "%{gcc_arch}-linux-%{abi}"
  triplet: "%{arch}-unknown-linux-%{abi}"
  gcc_arch: "%{arch}"
  abi: "gnu"
  common_flags: "-O2 -g -pipe -Wp,-D_FORTIFY_SOURCE=2 -Wp,-D_GLIBCXX_ASSERTIONS -fexceptions -fstack-protector-strong -grecord-gcc-switches"
  flags_x86_64: "-march=x86-64 -mtune=generic %{common_flags} -fasynchronous-unwind-tables -fstack-clash-protection -fcf-protection"
  flags_i686: "-march=i686 -mtune=generic %{common_flags} -fasynchronous-unwind-tables -fstack-clash-protection -fcf-protection"
  flags_aarch64: "%{common_flags} -fasynchronous-unwind-tables -fstack-clash-protection"
  flags_arm: "%{common_flags}"
  ldflags_defaults: "-Wl,-z,relro,-z,now -Wl,--as-needed"
  (?):
    - target_arch == "i686":
        gcc_arch: "i386"
    - target_arch == "arm":
        abi: "gnueabihf"
  ca_path: "%{sysconfdir}/ssl/certs/ca-certificates.crt"

  strip-binaries: |
    touch source-files
    find "%{install-root}" -type f \
      '(' -perm -111 -o -name '*.so*' \
          -o -name '*.cmxs' -o -name '*.node' ')' \
          -print0 | while read -r -d $'\0' file; do
      read -n4 hdr <"${file}" || continue # check for elf header
      if [ "$hdr" != "$(printf \\x7fELF)" ]; then
        continue
      fi
      if objdump -j .gnu_debuglink -s "${file}" &>/dev/null; then
        continue
      fi
      case "${file}" in
        "%{install-root}%{debugdir}/"*)
          continue
          ;;
        *)
          ;;
      esac
      realpath="$(realpath -s --relative-to="%{install-root}" "${file}")"
      debugfile="%{install-root}%{debugdir}/${realpath}.debug"
      mkdir -p "$(dirname "$debugfile")"
      debugedit -i --list-file=source-files.part --base-dir="%{build-root}" --dest-dir="%{debugdir}/source/%{element-name}" "${file}"
      cat source-files.part >>source-files
      objcopy %{objcopy-extract-args} "${file}" "$debugfile"
      chmod 644 "$debugfile"
      mode="$(stat -c 0%a "${file}")"
      [ -w "${file}" ] || chmod +w "${file}"
      strip %{strip-args} "${file}"
      objcopy %{objcopy-link-args} "$debugfile" "${file}"
      chmod "${mode}" "${file}"
    done
    sort -zu  <source-files | while read -r -d $'\0' source; do
      dst="%{install-root}%{debugdir}/source/%{element-name}/${source}"
      src="%{build-root}/${source}"
      if [ -d "${src}" ]; then
        install -m0755 -d "${dst}"
        continue
      fi
      [ -f "${src}" ] || continue
      install -m0644 -D "${src}" "${dst}"
    done

environment:
  (?):
    - target_arch == "x86_64":
        CFLAGS:  "%{flags_x86_64}"
        CXXFLAGS: "%{flags_x86_64}"
        LDFLAGS:  "%{ldflags_defaults}"
    - target_arch == "i686":
        CFLAGS: "%{flags_i686}"
        CXXFLAGS: "%{flags_i686}"
        LDFLAGS:  "%{ldflags_defaults}"
    - target_arch == "arm":
        CFLAGS:  "%{flags_arm}"
        CXXFLAGS: "%{flags_arm}"
        LDFLAGS:  "%{ldflags_defaults}"
    - target_arch == "aarch64":
        CFLAGS:  "%{flags_aarch64}"
        CXXFLAGS: "%{flags_aarch64}"
        LDFLAGS:  "%{ldflags_defaults}"
  LC_ALL: en_US.UTF-8
  PYTHON: "%{bindir}/python3"

  # To work-around some issues with Fedora, instead of using ldconfig,
  # libtool reads ld.so.conf to find paths that do not need RPATH.
  # Unfortunately we do not write in ld.so.conf because flatpak
  # expects it empty.
  # We do not use variable %{libdir} or %{prefix} here because elements
  # might redefine those variables. We want LT_SYS_LIBRARY_PATH to have
  # the value that was used for binutils for %{libdir}, so we expand
  # the value.
  LT_SYS_LIBRARY_PATH: "/usr/lib/%{gcc_triplet}"

split-rules:
  devel:
    - "%{includedir}"
    - "%{includedir}/**"
    - "%{libdir}/pkgconfig"
    - "%{libdir}/pkgconfig/**"
    - "%{datadir}/pkgconfig"
    - "%{datadir}/pkgconfig/**"
    - "%{datadir}/aclocal"
    - "%{datadir}/aclocal/**"
    - "%{prefix}/lib/cmake"
    - "%{prefix}/lib/cmake/**"
    - "%{libdir}/cmake"
    - "%{libdir}/cmake/**"
    - "%{prefix}/lib/*.a"
    - "%{libdir}/*.a"

  debug:
    - "%{debugdir}/**"

plugins:
  - origin: local
    path: plugins/sources
    sources:
      crate: 0

  - origin: pip
    package-name: buildstream-external
    elements:
      collect_integration: 0
      flatpak_image: 0
      x86image: 0
    sources:
      git_tag: 0

options:
  bootstrap_build_arch:
    type: arch
    description: Architecture
    variable: bootstrap_build_arch
    values:
      - arm
      - aarch64
      - i686
      - x86_64

  target_arch:
    type: arch
    description: Architecture
    variable: arch
    values:
      - arm
      - aarch64
      - i686
      - x86_64

artifacts:
  url: https://freedesktop-sdk-cache.codethink.co.uk:11001

elements:
  cmake:
    variables:
      generator: Ninja
  autotools:
    variables:
      remove_libtool_modules: "true"
      remove_libtool_libraries: "true"
      delete_libtool_files: |
          find "%{install-root}" -name "*.la" -print0 | while read -d '' -r file; do
            if grep '^shouldnotlink=yes$' "${file}" &>/dev/null; then
              if %{remove_libtool_modules}; then
                echo "Removing ${file}."
                rm "${file}"
              else
                echo "Not removing ${file}."
              fi
            else
              if %{remove_libtool_libraries}; then
                echo "Removing ${file}."
                rm "${file}"
              else
                echo "Not removing ${file}."
              fi
            fi
          done
      conf-global: |
        %{conf-deterministic} \
        %{conf-link-args} \
        %{conf-build} \
        %{conf-host}
      conf-cmd: configure
    config:
      configure-commands:
        - |
          %{autogen}
          if [ -n "%{builddir}" ]; then
            mkdir %{builddir}
            cd %{builddir}
              reldir=..
            else
              reldir=.
          fi
          ${reldir}/%{configure}

      build-commands:
        - |
          if [ -n "%{builddir}" ]; then
            cd %{builddir}
          fi
          %{make}

      install-commands:
        - |
          if [ -n "%{builddir}" ]; then
            cd %{builddir}
          fi
          %{make-install}

        - |
          %{delete_libtool_files}

sources:
  git_tag:
    config:
      track-tags: True
